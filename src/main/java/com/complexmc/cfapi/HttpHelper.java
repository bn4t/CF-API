package com.complexmc.cfapi;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import java.io.IOException;


public class HttpHelper {

    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static OkHttpClient client = new OkHttpClient();

    /**
     * Send a POST request.
     *
     * @param targetUrl URL for the HTTP request
     * @param data      data sent with the post request
     * @return returns the HTTP response from the server
     * @throws IOException Throws IO Exception
     */
    public static String sendPostRequest(String targetUrl, String data) throws IOException {

        RequestBody body = RequestBody.create(JSON, data);
        Request request = new Request.Builder()
                .url(targetUrl)
                .addHeader("X-Auth-Email", CFapi.authEmail)
                .addHeader("X-Auth-Key", CFapi.authKey)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();

        assert response.body() != null;
        return response.body().string();
    }


    /**
     * Send a GET request.
     *
     * @param targetUrl URL for the HTTP request
     * @return returns the HTTP response from the server
     * @throws IOException Throws IO Exception
     */
    public static String sendGetRequest(String targetUrl) throws IOException {

        Request request = new Request.Builder()
                .url(targetUrl)
                .addHeader("X-Auth-Email", CFapi.authEmail)
                .addHeader("X-Auth-Key", CFapi.authKey)
                .get()
                .build();
        Response response = client.newCall(request).execute();

        assert response.body() != null;
        return response.body().string();
    }


    /**
     * Send a PUT request.
     *
     * @param targetUrl URL for the HTTP request
     * @param data      data sent with the post request
     * @return returns the HTTP response from the server
     * @throws IOException Throws IO Exception
     */
    public static String sendPutRequest(String targetUrl, String data) throws IOException {

        RequestBody body = RequestBody.create(JSON, data);

        Request request = new Request.Builder()
                .url(targetUrl)
                .addHeader("X-Auth-Email", CFapi.authEmail)
                .addHeader("X-Auth-Key", CFapi.authKey)
                .put(body)
                .build();
        Response response = client.newCall(request).execute();

        assert response.body() != null;
        return response.body().string();
    }


    /**
     * Send a DELETE request.
     *
     * @param targetUrl URL for the HTTP request
     * @return returns the HTTP response from the server
     * @throws IOException Throws IO Exception
     */
    public static String sendDeleteRequest(String targetUrl) throws IOException {

        Request request = new Request.Builder()
                .url(targetUrl)
                .addHeader("X-Auth-Email", CFapi.authEmail)
                .addHeader("X-Auth-Key", CFapi.authKey)
                .delete()
                .build();
        Response response = client.newCall(request).execute();

        assert response.body() != null;
        return response.body().string();
    }


    /**
     * Send a PATCH request.
     *
     * @param targetUrl URL for the HTTP request
     * @return returns the HTTP response from the server
     * @throws IOException Throws IO Exception
     */
    public static String sendPatchRequest(String targetUrl, String data)
            throws IOException {

        RequestBody body = RequestBody.create(JSON, data);

        Request request = new Request.Builder()
                .url(targetUrl)
                .addHeader("X-Auth-Email", CFapi.authEmail)
                .addHeader("X-Auth-Key", CFapi.authKey)
                .patch(body)
                .build();
        Response response = client.newCall(request).execute();

        assert response.body() != null;
        return response.body().string();
    }
}