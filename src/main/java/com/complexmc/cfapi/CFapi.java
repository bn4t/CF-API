package com.complexmc.cfapi;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

@SuppressWarnings("unused")
public class CFapi {

    public static String authEmail;
    public static String authKey;
    private static boolean credentialsSet = false;

    private static final String ERROR_NO_CREDENTIALS_DEFINED =
            "Error: No credentials defined!";

    private static final String CF_API_URL =
            "https://api.cloudflare.com/client/v4/";


    /**
     * Stores the Cloudflare credentials in variables for later use.
     *
     * @param email        email adress of the used account
     * @param globalApiKey API Key of the used account
     **/
    public static void setCredentials(String email, String globalApiKey) {
        authEmail = email;
        authKey = globalApiKey;
        credentialsSet = true;
    }


    /**
     * Creates a new DNS record.
     *
     * @param zoneIdentifier ID of the corresponding DNS zone
     * @param type           type of the DNS record (ex. A)
     * @param name           name of the DNS record
     * @param content        target address of the DNS record
     * @param ttl            TimeToLive of the DNS record
     * @param proxied        select if the DNS record should be proxied through Cloudflare
     * @return returns the API response as String
     **/
    public static String createDnsRecord(String zoneIdentifier, String type, String name,
                                         String content, Integer ttl, Boolean proxied) {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        try {
            return HttpHelper.sendPostRequest(CF_API_URL
                            + "zones/"
                            + zoneIdentifier
                            + "/dns_records",
                    "{\"type\":\""
                            + type
                            + "\",\"name\":\""
                            + name
                            + "\",\"content\":\""
                            + content
                            + "\",\"ttl\":"
                            + ttl
                            + ",\"proxied\":"
                            + proxied
                            + "}");
        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * List DNS records for a zone.
     *
     * @param zoneIdentifier ID of the corresponding DNS zone
     * @param type           type of the DNS records that should be listed
     * @param name           name of the DNS records that should be listed
     * @param content        target address of the DNS records that should be listed
     * @param direction      direction to order domains
     * @param entriesPerPage amount of entries per page
     * @param order          parameter to set the order of the list
     * @param page           number of the displayed page
     * @param match          whether to match all search requirements or at least one (any)
     * @return returns the API response as String
     **/
    public static String listDnsRecords(String zoneIdentifier, String type, String name,
                                        String content, Integer page, Integer entriesPerPage,
                                        String order, String direction, String match) {
        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        String strType = type != null ? "type=" + type : "";
        String strName = name != null ? "&name=" + name : "";
        String strContent = content != null ? "&content=" + content : "";
        String strPage = page != null ? "&page=" + page : "";
        String strEntriesPerPage = entriesPerPage != null ? "&per_page=" + entriesPerPage : "";
        String strOrder = order != null ? "&order=" + order : "";
        String strDirection = direction != null ? "&direction=" + page : "";
        String strMatch = match != null ? "&match=" + match : "";

        try {
            return HttpHelper.sendGetRequest(CF_API_URL
                    + "zones/"
                    + zoneIdentifier
                    + "/dns_records?"
                    + strType
                    + strName
                    + strContent
                    + strPage
                    + strEntriesPerPage
                    + strOrder
                    + strDirection
                    + strMatch);
        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Get Details of a DNS record.
     *
     * @param zoneIdentifier   ID of the corresponding DNS zone
     * @param recordIdentifier ID of the selected DNS record
     * @return returns the API response as String
     **/
    public static String getDnsRecordDetails(String zoneIdentifier, String recordIdentifier) {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        try {
            return HttpHelper.sendGetRequest(CF_API_URL
                    + "zones/"
                    + zoneIdentifier
                    + "/dns_records/"
                    + recordIdentifier);

        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Update a DNS record.
     *
     * @param zoneIdentifier   ID of the corresponding DNS zone
     * @param type             type of the DNS record (ex. A)
     * @param name             name of the DNS record
     * @param content          target address of the DNS record
     * @param ttl              TimeToLive of the DNS record
     * @param proxied          select if the DNS record should be proxied through Cloudflare
     * @param recordIdentifier ID of the DNS record
     * @return returns the API response as String
     **/
    public static String updateDnsRecord(String zoneIdentifier, String recordIdentifier,
                                         String type, String name, String content,
                                         Integer ttl, Boolean proxied) {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        String strTtl = ttl != null ? "\"ttl\":" + ttl : "";
        String strProxied = proxied != null ? "\"proxied\":" + proxied : "";

        try {
            return HttpHelper.sendPutRequest(CF_API_URL
                            + "zones/"
                            + zoneIdentifier
                            + "/dns_records/"
                            + recordIdentifier,

                    "{\"type\":\""
                            + type
                            + "\",\"name\":\""
                            + name + "\",\"content\":\""
                            + content
                            + "\","
                            + strTtl
                            + ","
                            + strProxied + "}");

        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Delete a DNS record.
     *
     * @param zoneIdentifier   ID of the corresponding DNS zone
     * @param recordIdentifier ID of the selected DNS record
     * @return returns the API response as String
     **/
    public static String deleteDnsRecord(String zoneIdentifier, String recordIdentifier) {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        try {
            return HttpHelper.sendDeleteRequest(CF_API_URL
                    + "zones/"
                    + zoneIdentifier
                    + "/dns_records/"
                    + recordIdentifier);

        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Create a new zone.
     *
     * @param domain       The domain of the zone
     * @param jumpStart    If Cloudflare should automatically attempt to fetch existing DNS records
     * @param organisation If the new zone should be owned by an organisation it has to be specified here
     * @return returns the API response as String
     **/
    public static String createZone(String domain, Boolean jumpStart,
                                    String organisation) {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        String strJumpStart = jumpStart != null ? ",\"jump_start\":" + jumpStart : "";
        String strOrganisation = organisation != null ? ",\"organisation\":" + organisation : "";

        try {
            return HttpHelper.sendPostRequest(CF_API_URL + "zones/",
                    "{\"name\":\""
                            + domain
                            + strJumpStart
                            + strOrganisation
                            + "}");

        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Initiate another zone activation check.
     *
     * @param zoneIdentifier The ID of the corresponding zone
     * @return returns the API response as String
     **/
    public static String initZoneActivationCheck(String zoneIdentifier) {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        try {
            return HttpHelper.sendPutRequest(CF_API_URL
                    + "zones/"
                    + zoneIdentifier

                    //empty data argument since no data is required
                    + "/activation_check/", "");

        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * List zones.
     *
     * @param name           Zone name
     * @param status         Status of the zone
     * @param direction      direction to order zones
     * @param entriesPerPage amount of entries per page
     * @param order          parameter to set the order of the list
     * @param page           number of the displayed page
     * @param match          whether to match all search requirements or at least one (any)
     * @return returns the API response as String
     **/
    public static String listZones(String name, String status, Integer page,
                                   Integer entriesPerPage, String order,
                                   String direction, String match) {
        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        String strName = name != null ? "&name=" + name : "";
        String strStatus = status != null ? "&status=" + status : "";
        String strPage = page != null ? "&page=" + page : "";
        String strEntriesPerPage = entriesPerPage != null ? "&per_page=" + entriesPerPage : "";
        String strOrder = order != null ? "&order=" + order : "";
        String strDirection = direction != null ? "&direction=" + page : "";
        String strMatch = match != null ? "&match=" + match : "";

        try {
            return HttpHelper.sendGetRequest(CF_API_URL
                    + "zones/"
                    + strName
                    + strStatus
                    + strPage
                    + strEntriesPerPage
                    + strOrder
                    + strDirection
                    + strMatch);
        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Get details of a zone.
     *
     * @param zoneIdentifier ID of corresponding zone
     * @return returns the API response as String
     **/
    public static String getZoneDetails(String zoneIdentifier) {
        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        try {
            return HttpHelper.sendGetRequest(CF_API_URL
                    + "zones/"
                    + zoneIdentifier);
        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Edit a zone
     *
     * @param zoneIdentifier    ID of the corresponding DNS zone
     * @param paused            Set if Cloudflare should be paused for this zone
     * @param vanityNameServers An array of domains used for custom name servers
     * @param plan              The desired plan for the zone
     * @return returns the API response as String
     **/
    public static String editZoneProperties(String zoneIdentifier, Boolean paused,
                                            Array vanityNameServers, String plan) {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        String strPaused = paused != null ? ",\"paused\":" + paused : "";
        String strVanityNameServers = vanityNameServers != null ? ",\"vanity_name_servers\":"
                + vanityNameServers.toString() : "";
        String strPlan = plan != null ? ",\"plan\":" + plan : "";


        try {
            return HttpHelper.sendPatchRequest(CF_API_URL
                            + "zones/"
                            + zoneIdentifier,

                    "{" + strPaused
                            + strVanityNameServers
                            + strPlan
                            + "}");

        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Delete a zone
     *
     * @param zoneIdentifier    ID of the corresponding DNS zone
     * @return returns the API response as String
     **/
    public static String deleteZone(String zoneIdentifier) {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }

        try {
            return HttpHelper.sendDeleteRequest(CF_API_URL
                            + "zones/"
                            + zoneIdentifier);

        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }


    /**
     * Get the details of the current user
     *
     * @return returns the API response as String
     **/
    public static String getUserDetails() {

        if (!credentialsSet) {
            return ERROR_NO_CREDENTIALS_DEFINED;
        }


        try {
            return HttpHelper.sendGetRequest(CF_API_URL
                    + "user");

        } catch (IOException e) {
            return Arrays.toString(e.getStackTrace());
        }
    }

}

