# Cloudflare-API
[![Build Status](https://travis-ci.org/bn4t/CF-API.svg?branch=master)](https://travis-ci.org/bn4t/CF-API)

## Features

- Create DNS records
- Delete DNS records
- List DNS records
- View details of DNS records
- Update DNS records
- Delete DNS records
- Create zones
- Edit zones
- Delete zones
- View current user info


## How it works
**Optional arguments are marked with `[optional]` and should be replaced with `null` when not used.**



### Set cloudflare credentials

````
CFapi.setCredentials(EMAIL,API_KEY);
````



### Create a dns record for your zone

````
CFapi.createDnsRecord(ZONE, RECORD_TYPE, RECORD_NAME, CONTENT, TTL, CF_PROXY);
````



### List DNS records for specified zone

````
CFapi.listDnsRecords(ZONE, [optional] RECORD_TYPE, [optional] RECORD_NAME, [optional] RECORD_CONTENT, [optional] PAGE, [optional] ENTRIES_PER_PAGE, [optional] ORDER, [optional] DIRECTION, [optional] MATCH);
````


### Get details of DNS record

````
CFapi.getDnsRecordDetails(ZONE, RECORD_ID);
````


### Update DNS record

````
CFapi.updateDnsRecord(ZONE, RECORD_ID, TYPE, NAME, CONTENT, [optional] TTL, [optional] CF_PROXY);
````

### Delete DNS record

````
CFapi.deleteDnsRecord(ZONE, RECORD_ID);
````

---

### Create zone

````
CFapi.createZone(DOMAIN, [optional] JUMPSTART, [optional] ORGANISATION);
````


### Initiate zone activation check

````
CFapi.initZoneActivationCheck(ZONE);
````


### List zones

````
CFapi.listZones( [optional] NAME, [optional] STATUS, [optional] PAGE, [optional] ENTRIES_PER_PAGE, [optional] ORDER, [optional] DIRECTION, [optional] MATCH);
````

### Get zone details

````
CFapi.getZoneDetails(ZONE);
````

### Edit a zone
````
CFapi.editZoneProperties(ZONE, [optional] PAUSED, [optional] VANITY_NAME_SERVERS, [optional] PLAN);
````

### Delete a zone
````
CFapi.deleteZone(ZONE);
````

---

### Get details of the current user
````
CFapi.getUserDetails();
````



## Installation

Download the jar and set it as dependency for your project.

## Dependencies

- okhttp

````
<dependency>
    <groupId>com.squareup.okhttp3</groupId>
    <artifactId>okhttp</artifactId>
    <version>3.9.1</version>
</dependency>
````
